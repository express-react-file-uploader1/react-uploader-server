const express = require("express");
// const fileUpload = require("express-fileupload");
const cors = require("cors");
// const fs = require("fs");
// const envConfig = require("./app/config/env.config");
const connectDB = require('./app/config/database');
const  upload = require('./app/routes/upload.routes')

connectDB();

const app = express();
app.use(cors());

// app.use(fileUpload());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static("public"));

app.get('/',(req,res)=>{
  res.send("Welcome")
})
require("./app/routes/auth.routes")(app);
require("./app/routes/user.routes")(app);
// require("./app/routes/upload.routes")(app);
app.use('/upload',upload)



const PORT =process.env.PORT || 5000 ;
app.listen(PORT, () => console.log("Server Started..."));
