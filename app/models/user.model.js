const mongoose =require('mongoose')

const User = mongoose.Schema({
  userName: {
    type : String
  },
  password : {
    type : String,
  },
  email : {
    type : String,
  },
  roles: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role"
    }
  ],
  uploads : [{
    type : mongoose.Schema.Types.ObjectId, 
    ref: 'Upload'
  }]
})

module.exports = User;