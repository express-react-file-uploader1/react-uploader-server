const mongoose =require('mongoose')

const Upload = mongoose.Schema({
  filename : {
    type : String
  },
  filesrc : {
    type :String 
  },
  tags : {
    type : [String],
  },
  author :{
    type : String
  },
  filetype : {
    type :String
  }
})

module.exports = Upload;