const mongoose = require("mongoose");
const envConfig = require("./env.config");
const db = require("../models");
const Role = db.role;


function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "moderator"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}

const connectDB = () => {
  db.mongoose.connect(envConfig.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  }).then(()=>{
    console.log(`MongoDB Connected: ${envConfig.MONGODB_URI}`);
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

};

module.exports = connectDB; 
