// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
const  firebase = require('firebase/app');
// import { initializeApp } from "firebase"
require('firebase/storage');



// For Firebase JS SDK v7.20.0 and later, measurementId is optional


const firebaseConfig = {
  apiKey: "AIzaSyBqSdc8ra5s78tSimHcaEpiMHxdFX0Ns1o",
  authDomain: "react-file-uploader-2729f.firebaseapp.com",
  projectId: "react-file-uploader-2729f",
  storageBucket: "react-file-uploader-2729f.appspot.com",
  messagingSenderId: "617899426544",
  appId: "1:617899426544:web:051717fac2e021dd8d3835",
  measurementId: "G-0F520LZZLH",
  databaseURL: "https://react-file-uploader-2729f.firebaseio.com",

};


const firebaseApp = firebase.initializeApp(firebaseConfig);
module.exports= firebaseApp
