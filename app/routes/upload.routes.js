const express = require("express");
const router = express.Router();
const db = require("../models");

router.get("/", async (req, res) => {
  res.send("Welcome to uploads/get");
});



router.post("/", async (req, res) => {
 
  const files = req.body.files
  let tags = req.body.tags;
  tags = tags.replace(" ","")
  tags = tags.split("#")
  if(tags[0]===""){
    tags.shift()
  }
  if(tags[tags.length-1]===""){
    tags.pop();
  }

  files.map(async file=>{
    const upload = await  db.upload.create({
      filename : file.fileName ,
      filesrc : file.fileSrc ,
      filetype : file.fileType ,
      tags :tags  
    })
    upload.save()
  })

  res.json(files)


  // if (req.files.files.length> 1) {
  //   const files = req.files.files;
  //   const fileUrls = req.body.fileUrls
  //   let arr = [];
  //   for(let i=0;i <req.files.files.length ;i++){
  //     arr.push({
  //           filename: files[i].name,
  //           filesrc: fileUrls[i],
  //           tags: tags,
  //           filetype: files[i].mimetype,
  //         });
  //   }

  //   arr.map(async (upload) => {
  //     const newUpload = await Upload.create(upload);
  //     newUpload.save();
  //   });
  //   res.json(arr);
  // } else {
  //   const file = req.files.files;
  //   const fileUrl = req.body.fileUrls
  //   const newUpload = await Upload.create({
  //     filename: file.name,
  //     tags: tags ,
  //     filesrc: fileUrl,
  //     filetype: file.mimetype,
  //   });

  //   newUpload.save();
 
  //   res.json({
  //     filename: file.name,
  //     filesrc: fileUrl ,
  //     filetype: file.mimetype,
  //   });
  // }
});

router.get("/:tag", (req, res) => {
  let tag = `#${req.params.tag}`;

  db.upload.find({ tags: tag }, (err, uploads) => {
    if (err) {
      console.log(err);
    } else {
      res.json(uploads);
    }
  });
});

module.exports = router;
